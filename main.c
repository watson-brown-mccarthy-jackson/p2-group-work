
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <stdbool.h>


//Order
/*
 * 1.Add the patient first before we can have the room
 * 2.Each id in the file must be unique.
 */


//***************************************************************
// Struct.
//***************************************************************

//Patient
struct Patient{
    int patientID;
    char firstName[40];
    char lastName[40];
    char covidStatus[40];
    int birthYear;
    char patientsHosStatus[40];
};

//Rooms
struct Room{
    int roomID;
    char respiratorCheck[40];
    char roomHelp[40];
    struct Patient patient;
};


//***************************************************************
// Functions
//***************************************************************
bool createFiles();
void NameEntered(const char *string);
void operationsP(const int *choice);
void operationsR(const int *choice);
void patientsOptions();
void roomsOptions();


//***************************************************************
// Main
//***************************************************************
int main() {

    //character pointer.
    char *fileName = (char*)malloc(10 *sizeof(char));

    //Checking if files were created.
    if(!createFiles()){
        printf("Error. Files were not created.\n");
        exit(EXIT_FAILURE);
    }// end if

    printf("\t\t\tWelcome User.\n");

    //In a infinite loop for now just to test.
    do {
        //asking user to enter the file name they want to edit.
        printf("\t\t\tEnter the name of the file you want to edit.\n");
        printf("\t\t\ttype patients for the patient file or rooms for the room file.\n");

        //checking filename pointer
        if (fileName == NULL) {
            printf("error allocating memory\n");
            exit(EXIT_FAILURE);
        }// end if

        scanf("%s", fileName);
        NameEntered(fileName);

    }while(true);
//    free(fileName);
    return 0;

}


//***************************************************************
// Will return true if files were created.
//***************************************************************
bool createFiles(){

    //Creating Files
    FILE *patientFilePtr = fopen("patients.txt", "w+");
    FILE *roomFilePtr = fopen("rooms.txt", "w+");

    bool flag = false;

    //checking if file pointers are null
    if(patientFilePtr != NULL && roomFilePtr != NULL){
        flag = true;
    }// end if

    //Closes the files.
    fclose(patientFilePtr);
    fclose(roomFilePtr);

    return flag;
}// end function


//***************************************************************
// checks the string value.
//***************************************************************
void NameEntered(const char *string){

    int choice;

    if(strcmp(string, "patients") == 0){
        system("cls");

        patientsOptions();
        scanf("%d", &choice);

        operationsP(&choice);
    }else if(strcmp(string, "rooms") == 0){
        system("cls");

        roomsOptions();
        scanf("%d", &choice);

        operationsR(&choice);

    } else{
        printf("Error\n");
    }// end else
}// end function


//***************************************************************
// operations for the patient files.
//***************************************************************
void operationsP(const int *choice){

    switch(*choice){
        case 1:

            system("cls");
            //ADD PATIENT HERE. Jackson
            //Check that the id does not already exist.
            //check if file exist if so open file.
            //then collect data from user the struct and storing in the file in a+ mode.
            //name of file is patients.
            break;

        case 2:
            break;

        case 3:
            break;

        case 4:
            break;

        default:
            printf("Invalid input\n");
            break;
    }// end switch

}// end function



//***************************************************************
// operations for room file.
//***************************************************************
void operationsR(const int *choice){
    switch(*choice){
        case 1:

            system("cls");
            //ADD ROOM HERE. McCarthy.
            //Check that the id does not already exist.
            //check if file exist if so open file.
            //then collect data from user the struct and storing in the file in a+ mode.
            //name of file is rooms.
            break;

        case 2:
            break;

        case 3:
            break;

        case 4:
            break;

        default:
            printf("Invalid input\n");
            break;
    }// end switch

}// end function

//***************************************************************
// Options for the patient file.
//***************************************************************
void patientsOptions(){
    printf("1.Add patient\n");
    printf("2.Go Back\n");
    printf("Enter: ");
}


//***************************************************************
// Options for the room file.
//***************************************************************
void roomsOptions(){
    printf("1.Add Room\n");
    printf("Enter: ");
}